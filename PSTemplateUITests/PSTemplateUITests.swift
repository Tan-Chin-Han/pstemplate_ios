//
//  PSTemplateUITests.swift
//  PSTemplateUITests
//
//  Created by Edimax on 2021/12/23.
//

import XCTest
import SwiftUI
import NetworkExtension

class PSTemplateUITests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
       // testAddAccessPointsFlow()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
//        let app = XCUIApplication()
//        app.launch()
//        addUIInterruptionMonitor(withDescription: "Sophos", handler: { alert in
//            alert.sheets.buttons["Allow While Using App"].tap()
//                  return true
//                })
//
//        app.tap()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        Springboard.deleteApp()
//        super.tearDown()
    }
   func testAccessLocation() throws {
        let app = XCUIApplication()
        app.launch()
        var alertPressed: Bool = false
        let handler = addUIInterruptionMonitor(withDescription: "Sophos") { [self] element -> Bool in
        alertPressed = clickButtons(element: element, text: "Allow While Using App")
            return alertPressed
        }
        wait {
            app.tap()
        }
        waitAndCheck {
            alertPressed
        }
    }
 /*   func testAccessFace() throws {
        let app = XCUIApplication()
        app.launch()
        var alertPressed: Bool = false
        let handler = addUIInterruptionMonitor(withDescription: "FaceID") { [self] element -> Bool in
        alertPressed = clickButtons(element: element, text: "OK")
            return alertPressed
        }
        wait {
            app.tap()
        }
        waitAndCheck {
            alertPressed
        }
    }
    func testAccessSSDP() throws {
        let app = XCUIApplication()
        app.launch()
        var alertPressed: Bool = false
        let handler = addUIInterruptionMonitor(withDescription: "SSDP") { [self] element -> Bool in
        alertPressed = clickButtons(element: element, text: "OK")
            return alertPressed
        }
        wait {
            app.tap()
        }
        waitAndCheck {
            alertPressed
        }
    }*/
    func clickButtons(element: XCUIElement, text: String) -> Bool {
        let allowButton = element.buttons[text]
        if allowButton.exists {
            allowButton.tap()
            return true
        }
        return false
    }
//    func testSystemDialog() {
//        let app = XCUIApplication()
//        let allowButtonPredicate = NSPredicate(format: "label == 'Allow While Using App'")
//        let alertHandler0 = addUIInterruptionMonitor(withDescription: "Allow ”Security” to use your location?") { (alert) -> Bool in
//
//                let alwaysAllowButton = alert.buttons.matching(allowButtonPredicate).element.firstMatch
//            if alwaysAllowButton.exists {
//                alert.buttons["Allow While Using App"].tap()
//                // Required to return focus to app
//                app.tap()
//                return true
//            } else {
//                return false
//            }
//        }
//
//        let alertHandler = addUIInterruptionMonitor(withDescription: "“Security“ would like to find and connect to devices on your local network.") { (alert) -> Bool in
//            if alert.buttons.matching(identifier: "OK").count > 0 {
//                alert.buttons["OK"].tap()
//                // Required to return focus to app
//                app.tap()
//                return true
//            } else {
//                return false
//            }
//        }
//
//
//        app.launch()
//        sleep(20)
//        removeUIInterruptionMonitor(alertHandler0)
//        removeUIInterruptionMonitor(alertHandler)
////        addUIInterruptionMonitor(withDescription: "Allow ”Security” to use your location?") { (alert) -> Bool in
//////            alert.buttons["Allow While Using App"].tap()
//////            return true
////
////            let button = alert.buttons["Allow While Using App"].firstMatch
////                if button.exists {
////                    button.tap()
////                    return true // The alert was handled
////                }
////
////                return false // The alert was not handled
////        }
////        sleep(10)
////        addUIInterruptionMonitor(withDescription: "“Security“ would like to find and connect to devices on your local network.") { (alert) -> Bool in
////            let button = alert.buttons["OK"]
////                if button.exists {
////                    button.tap()
////                    return true // The alert was handled
////                }
////
////                return false // The alert was not handled
////        }
////        sleep(10)
//    }
    
    func testAddAccessPointsFlow() {
        let app = XCUIApplication()
        app.launch()
        takeScreenshot(name: "SplashScreen")
      
        let newAP = app.buttons["AddAllNewPressed"] // .accessibilityIdentifier("pointbuttonPressed")
        XCTAssertTrue(newAP.waitForExistence(timeout: 120))
        if newAP.isHittable {
        newAP.tap()
        }
        let addallaps = app.buttons["addallaps"] // .accessibilityIdentifier("pointbuttonPressed")
        XCTAssertTrue(addallaps.waitForExistence(timeout: 120))
        if addallaps.isHittable {
        addallaps.tap()
        }
        let myAP = app.buttons["myAPPressed"]
        XCTAssertTrue(myAP.waitForExistence(timeout: 120))
        if myAP.isHittable {
        myAP.tap()
        }
        let newAPPressed = app.buttons["newAPPressed"]
        XCTAssertTrue(newAPPressed.waitForExistence(timeout: 120))
        if newAPPressed.isHittable {
            newAPPressed.tap()
        }
        if myAP.isHittable {
            myAP.tap()
        }
        
        let seachBar = app.otherElements.textFields["seachBar"]
        XCTAssertTrue(seachBar.waitForExistence(timeout: 120))
        seachBar.tap() //.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if seachBar.isHittable {
            seachBar.typeText("420X")
        }
        sleep(5)
        
//        sleep(10)
        let searchX = app.buttons["searchX"].firstMatch
        XCTAssertTrue(searchX.waitForExistence(timeout: 120))
        searchX.tap()
        
        sleep(5)
        let search = app.buttons["search"].firstMatch
        XCTAssertTrue(search.waitForExistence(timeout: 120))
        search.tap()
        sleep(5)
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        sleep(5)
        
        let name = app.otherElements.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if name.isHittable {
            name.typeText("admin")
        }
        
        sleep(5)
        
        let password = app.otherElements.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
        
        let system = app.buttons["system"].firstMatch
        XCTAssertTrue(system.waitForExistence(timeout: 120))
        system.tap()
        
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        
        app.images["back20"].tap()
        sleep(5)
        
        let lanport = app.buttons["lanport"].firstMatch
        XCTAssertTrue(lanport.waitForExistence(timeout: 120))
        lanport.tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        
        let laniptype = app.buttons["laniptype"].firstMatch
        XCTAssertTrue(laniptype.waitForExistence(timeout: 120))
        laniptype.tap()
        
        sleep(5)
        
        let laniptypeone = app.buttons["laniptypeoneStatic IP"].firstMatch
        XCTAssertTrue(laniptypeone.waitForExistence(timeout: 120))
        laniptypeone.tap()
        
        sleep(5)
        
        let ipaddress = app.textFields["ipaddress"]
        XCTAssertTrue(ipaddress.waitForExistence(timeout: 120))
        ipaddress.tap()
        sleep(5)
        app.images["back20"].tap()
        
        sleep(5)
        
       /* app.images["back20"].tap()
        sleep(5)
        let myDevice6G = app.buttons["checkLogin00:AA:BB:CC:DD:10"].firstMatch
        XCTAssertTrue(myDevice6G.waitForExistence(timeout: 120))
        myDevice6G.tap()
        sleep(5)
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if name.isHittable {
            name.typeText("admin")
        }
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if password.isHittable {
            password.typeText("1234")
        }
        login.tap()
        sleep(5)
        
        let bands = app.buttons["Wireless Bands"].firstMatch
        XCTAssertTrue(bands.waitForExistence(timeout: 120))
        bands.tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        app.images["back20"].tap()
        sleep(5)*/
        
        let clients = app.buttons["Wireless Clients"].firstMatch
        XCTAssertTrue(clients.waitForExistence(timeout: 120))
        clients.tap()
        sleep(5)
        
        let client = app.buttons["client"].firstMatch
        XCTAssertTrue(client.waitForExistence(timeout: 120))
        client.swipeLeft()
        sleep(10)
        
        let deleteclient0 = app.buttons["deleteclient"].firstMatch
//
        XCTAssertTrue(deleteclient0.waitForExistence(timeout: 120))
        deleteclient0.tap()
//        deleteclient0.tapAtPosition(position: CGPoint(x: 342.3, y: 5.33))
//        let deleteclient = client.descendants(matching: .button).buttons["deleteclient"]
//        XCTAssertTrue(deleteclient.waitForExistence(timeout: 120))
//        deleteclient.tapAtPosition(position: CGPoint(x: 342.3, y: 5.33))
        sleep(5)
        
        let connectclients = app.buttons["connectclients"].firstMatch
        XCTAssertTrue(connectclients.waitForExistence(timeout: 120))
        connectclients.tap()
        sleep(5)
        
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        let date = app.buttons["Date And Time"].firstMatch
        XCTAssertTrue(date.waitForExistence(timeout: 120))
        date.tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        app.images["back20"].tap()
        
        sleep(5)
        app.images["back20"].tap()
        
        sleep(5)
        let central = app.buttons["checkLogin00:00:1e:00:01:af"]
        
        XCTAssertTrue(central.waitForExistence(timeout: 120))
        central.tap()
        sleep(10)
        app.images["back20"].tap()
        
        sleep(10)

    }
/*    func testCallRebootFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(120)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        app.buttons["reboot"].tap()
        sleep(5)
        app.buttons["rebootok"].tap()
        sleep(10)
        app.buttons["rebootokok"].tap()
        sleep(130)
        
    }
    func testDetailBandsFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let SSID2GNUM = app.buttons["SSID2GNUM"].firstMatch
        XCTAssertTrue(SSID2GNUM.waitForExistence(timeout: 120))
        SSID2GNUM.tap()
        sleep(10)
        
        let SSID2GNUM8 = app.buttons["SSID2GNUM2"].firstMatch
        XCTAssertTrue(SSID2GNUM8.waitForExistence(timeout: 120))
        SSID2GNUM8.tap()
        sleep(10)
        
        
        let SSID2GAuth1 = app.buttons["SSID2GAuth1"].firstMatch
        XCTAssertTrue(SSID2GAuth1.waitForExistence(timeout: 120))
        SSID2GAuth1.tap()
        sleep(10)
        
        let auth0 = app.buttons["auth0"].firstMatch
        XCTAssertTrue(auth0.waitForExistence(timeout: 120))
        auth0.tap()
        sleep(5)
        
        
        SSID2GAuth1.tap()
        sleep(5)
        let auth1 = app.buttons["auth1"].firstMatch
        XCTAssertTrue(auth1.waitForExistence(timeout: 120))
        auth1.tap()
        sleep(5)
        
        let psk2G2 = app.textFields["psk2G2"]
        XCTAssertTrue(psk2G2.waitForExistence(timeout: 120))
        psk2G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk2G2.clearText()
        psk2G2.typeText("Edimax1234")
        sleep(5)
        
        let SSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(SSIDscroll.waitForExistence(timeout: 120))
        SSIDscroll.swipeUp()
        sleep(5)
        
        let psk2G3 = app.textFields["psk2G3"]
        XCTAssertTrue(psk2G3.waitForExistence(timeout: 120))
        psk2G3.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk2G3.typeText("1")
        sleep(5)
        
        SSIDscroll.swipeUp()
        
        let psk2G6 = app.textFields["psk2G6"]
        XCTAssertTrue(psk2G6.waitForExistence(timeout: 120))
        psk2G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk2G6.clearText()
        psk2G6.typeText("Edimax1234")
        sleep(5)
        SSIDscroll.swipeUp()
        sleep(5)
        let psk2G7 = app.textFields["psk2G7"]
        XCTAssertTrue(psk2G7.waitForExistence(timeout: 120))
        psk2G7.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        SSIDscroll.swipeUp()
        
        sleep(5)
        
        //5g
        let SSID5GNUM = app.buttons["SSID5GNUM"].firstMatch
        XCTAssertTrue(SSID5GNUM.waitForExistence(timeout: 120))
        SSID5GNUM.tap()
        sleep(10)
        
    //    let SSID5GNUM8 = app.buttons["SSID5GNUM2"].firstMatch
    //    XCTAssertTrue(SSID5GNUM8.waitForExistence(timeout: 120))
        SSID2GNUM8.tap()
        sleep(10)
        
        
        let SSID5GAuth1 = app.buttons["SSID5GAuth1"].firstMatch
        XCTAssertTrue(SSID5GAuth1.waitForExistence(timeout: 120))
        SSID5GAuth1.tap()
        sleep(10)
        
        //let auth0 = app.buttons["auth0"].firstMatch
        //XCTAssertTrue(auth0.waitForExistence(timeout: 120))
        auth0.tap()
        sleep(5)
        
        SSID5GAuth1.tap()
        sleep(5)
        
        auth1.tap()
        sleep(5)
        
        let psk5G2 = app.textFields["psk5G2"]
        XCTAssertTrue(psk5G2.waitForExistence(timeout: 120))
        psk5G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk5G2.clearText()
        psk5G2.typeText("Edimax1234")
        sleep(5)
        let psk5G3 = app.textFields["psk5G3"]
        XCTAssertTrue(psk5G3.waitForExistence(timeout: 120))
        psk5G3.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        SSIDscroll.swipeUp()
        sleep(5)
        let psk5G6 = app.textFields["psk5G6"]
        XCTAssertTrue(psk5G6.waitForExistence(timeout: 120))
        psk5G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        psk5G6.clearText()
        sleep(5)
        psk5G6.typeText("Edimax1234")
        sleep(5)
        
        SSIDscroll.swipeUp()
        
        sleep(5)
        
        app.images["back20"].tap()
        sleep(5)
     /*   app.images["back20"].tap()
        sleep(5)
        
       
        let myDevice6G = app.buttons["checkLogin00:AA:BB:CC:DD:10"].firstMatch
        XCTAssertTrue(myDevice6G.waitForExistence(timeout: 120))
        myDevice6G.tap()
        sleep(5)
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if name.isHittable {
            name.typeText("admin")
        }
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if password.isHittable {
            password.typeText("1234")
        }
        login.tap()
        sleep(5)
        
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        SSIDscroll.swipeUp()
        sleep(5)
        
        //6g
        let SSID6GNUM = app.buttons["SSID6GNUM"].firstMatch
        XCTAssertTrue(SSID6GNUM.waitForExistence(timeout: 120))
        SSID6GNUM.tap()
        sleep(10)
        
     //   let SSID2GNUM8 = app.buttons["SSID2GNUM2"].firstMatch
     //   XCTAssertTrue(SSID2GNUM8.waitForExistence(timeout: 120))
        SSID2GNUM8.tap()
        sleep(10)
        
        
        let SSID6GAuth1 = app.buttons["SSID6GAuth1"].firstMatch
        XCTAssertTrue(SSID6GAuth1.waitForExistence(timeout: 120))
        SSID6GAuth1.tap()
        sleep(10)
        
        let auth06G = app.buttons["auth2"].firstMatch
        XCTAssertTrue(auth06G.waitForExistence(timeout: 120))
        auth06G.tap()
        sleep(5)
        SSIDscroll.swipeUp()
        sleep(5)
       // let auth0 = app.buttons["auth0"].firstMatch
      //  XCTAssertTrue(auth0.waitForExistence(timeout: 120))
        
        SSID6GAuth1.tap()
        sleep(5)
        let auth16G = app.buttons["auth1"].firstMatch
        XCTAssertTrue(auth16G.waitForExistence(timeout: 120))
        auth16G.tap()
        sleep(5)
        
        let psk6G2 = app.textFields["psk6G2"]
        XCTAssertTrue(psk6G2.waitForExistence(timeout: 120))
        psk6G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk6G2.clearText()
        psk6G2.typeText("Edimax1234")
        sleep(5)
        SSIDscroll.swipeUp()
        sleep(5)
        let psk6G6 = app.textFields["psk6G6"]
        XCTAssertTrue(psk6G6.waitForExistence(timeout: 120))
        psk6G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk6G6.clearText()
        psk6G6.typeText("Edimax1234")
        sleep(5)
        //
        app.images["back20"].tap()
        sleep(20)
        
        funcbands.tap()
        sleep(5)*/
        
        app.buttons["Edit"].tap()
        sleep(5)
        app.buttons["Save"].tap()
        sleep(80)
        
    }
    func testDetailBandsoff2GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let Bands2G = app.switches["Bands2G"].firstMatch
        XCTAssertTrue(Bands2G.waitForExistence(timeout: 120))
        Bands2G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(5)
        
        app.buttons["Save"].tap()
        sleep(60)
        
    }
    func testDetailBandsoff5GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
//        let SSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
//        XCTAssertTrue(SSIDscroll.waitForExistence(timeout: 120))
//        SSIDscroll.swipeUp()
//
//        sleep(5)
        
        let Bands5G = app.switches["Bands5G"].firstMatch
        XCTAssertTrue(Bands5G.waitForExistence(timeout: 120))
        Bands5G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(5)
        
        app.buttons["Save"].tap()
        sleep(60)
        
    }
   /* func testDetailBandsoff6GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DD:10"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let SSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(SSIDscroll.waitForExistence(timeout: 120))
        SSIDscroll.swipeUp()
        
        sleep(5)
        
        let Bands6G = app.switches["Bands6G"].firstMatch
        XCTAssertTrue(Bands6G.waitForExistence(timeout: 120))
        Bands6G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(5)
        
        app.buttons["Save"].tap()
        sleep(60)
        
    }*/
    func testDetailBandson2GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let Bands2G = app.switches["Bands2G"].firstMatch
        XCTAssertTrue(Bands2G.waitForExistence(timeout: 120))
        Bands2G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(5)
        
        app.buttons["Save"].tap()
        sleep(120)
        
    }
    func testDetailBandson5GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
//        let SSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
//        XCTAssertTrue(SSIDscroll.waitForExistence(timeout: 120))
//        SSIDscroll.swipeUp()
//
//        sleep(5)
        
        let Bands5G = app.switches["Bands5G"].firstMatch
        XCTAssertTrue(Bands5G.waitForExistence(timeout: 120))
        Bands5G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(5)
        
        app.buttons["Save"].tap()
        sleep(120)
        
    }
   /* func testDetailBandson6GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DD:10"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let SSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(SSIDscroll.waitForExistence(timeout: 120))
        SSIDscroll.swipeUp()
        
        sleep(5)
        
        let Bands6G = app.switches["Bands6G"].firstMatch
        XCTAssertTrue(Bands6G.waitForExistence(timeout: 120))
        Bands6G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(5)
        
        app.buttons["Save"].tap()
        sleep(120)
        
    }*/
    func testDetailClientsFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        let wirelessclients = app.buttons["Funcclients"].firstMatch
        XCTAssertTrue(wirelessclients.waitForExistence(timeout: 120))
        wirelessclients.tap()
        sleep(10)
        
    }
    func testDetailDateAndTimeFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        app.buttons["Date And Time"].tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        
        let Datescroll = app.scrollViews["Datescroll"].firstMatch
        XCTAssertTrue(Datescroll.waitForExistence(timeout: 120))
        Datescroll.swipeUp()
        sleep(5)
        
        let Opentimezone = app.buttons["Opentimezone"].firstMatch
        XCTAssertTrue(Opentimezone.waitForExistence(timeout: 120))
        Opentimezone.tap()
        sleep(5)
        
        
        let zone = app.buttons["zone(GMT-06:00) Central Time (US & Canada)"].firstMatch
        XCTAssertTrue(zone.waitForExistence(timeout: 120))
        zone.tap()
        sleep(5)
        
        let NTPTimeServerHours = app.textFields["NTPTimeServerHours"]
        XCTAssertTrue(NTPTimeServerHours.waitForExistence(timeout: 120))
        NTPTimeServerHours.tap()
        sleep(5)
        
        let FromNTPServerName = app.textFields["FromNTPServerName"]
        XCTAssertTrue(FromNTPServerName.waitForExistence(timeout: 120))
        FromNTPServerName.tap()
        sleep(5)
        
        let NTPServerType = app.buttons["NTPServerType"].firstMatch
        XCTAssertTrue(NTPServerType.waitForExistence(timeout: 120))
        NTPServerType.tap()
        sleep(5)
        
        let NTPServerType0 = app.buttons["NTPServerTypeGlobal"].firstMatch
        XCTAssertTrue(NTPServerType0.waitForExistence(timeout: 120))
        NTPServerType0.tap()
        sleep(5)
        
        
        let ToggleDaylightSaving = app.buttons["ToggleDaylightSaving"].firstMatch
        XCTAssertTrue(ToggleDaylightSaving.waitForExistence(timeout: 120))
        ToggleDaylightSaving.tap()
        sleep(5)
        
        ToggleDaylightSaving.tap()
        sleep(5)
        
        let ToggleNTPServer = app.switches["ToggleNTPServer"].firstMatch
        XCTAssertTrue(ToggleNTPServer.waitForExistence(timeout: 120))
        ToggleNTPServer.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(10)
        //let Datescroll = app.scrollViews["Datescroll"].firstMatch
        //XCTAssertTrue(Datescroll.waitForExistence(timeout: 120))
        Datescroll.swipeDown()
        sleep(5)
        
        
        
        let TimeFromYourPhone = app.buttons["TimeFromYourPhone"].firstMatch
        XCTAssertTrue(TimeFromYourPhone.waitForExistence(timeout: 120))
        TimeFromYourPhone.tap()
        sleep(5)
        
        let LocalTime0 = app.buttons["LocalTime0"].firstMatch
        XCTAssertTrue(LocalTime0.waitForExistence(timeout: 120))
        LocalTime0.tap()
        sleep(5)
        
        let LocalTimeYear = app.buttons["LocalTimeYear2023"].firstMatch
        XCTAssertTrue(LocalTimeYear.waitForExistence(timeout: 120))
        LocalTimeYear.tap()
        sleep(5)
        
        let LocalTime1 = app.buttons["LocalTime1"].firstMatch
        XCTAssertTrue(LocalTime1.waitForExistence(timeout: 120))
        LocalTime1.tap()
        sleep(5)
        
        let LocalTimeMonth = app.buttons["LocalTimeMonthMarch"].firstMatch
        XCTAssertTrue(LocalTimeMonth.waitForExistence(timeout: 120))
        LocalTimeMonth.tap()
        sleep(5)
        
        let LocalTime2 = app.buttons["LocalTime2"].firstMatch
        XCTAssertTrue(LocalTime2.waitForExistence(timeout: 120))
        LocalTime2.tap()
        sleep(5)
        
        let LocalTimeDay = app.buttons["LocalTimeDay15"].firstMatch
        XCTAssertTrue(LocalTimeDay.waitForExistence(timeout: 120))
        LocalTimeDay.tap()
        sleep(5)
        
        let LocalTime3 = app.buttons["LocalTime3"].firstMatch
        XCTAssertTrue(LocalTime3.waitForExistence(timeout: 120))
        LocalTime3.tap()
        sleep(5)
        
        let LocalTimeHours = app.buttons["LocalTimeHours15"].firstMatch
        XCTAssertTrue(LocalTimeHours.waitForExistence(timeout: 120))
        LocalTimeHours.tap()
        sleep(5)
        
        let LocalTime4 = app.buttons["LocalTime4"].firstMatch
        XCTAssertTrue(LocalTime4.waitForExistence(timeout: 120))
        LocalTime4.tap()
        sleep(5)
        
        let LocalTimeMinutes = app.buttons["LocalTimeMinutes15"].firstMatch
        XCTAssertTrue(LocalTimeMinutes.waitForExistence(timeout: 120))
        LocalTimeMinutes.tap()
        sleep(5)
        
        let LocalTime5 = app.buttons["LocalTime5"].firstMatch
        XCTAssertTrue(LocalTime5.waitForExistence(timeout: 120))
        LocalTime5.tap()
        sleep(5)
        
        let LocalTimeSeconds = app.buttons["LocalTimeSeconds15"].firstMatch
        XCTAssertTrue(LocalTimeSeconds.waitForExistence(timeout: 120))
        LocalTimeSeconds.tap()
        sleep(5)
        
        app.images["back20"].tap()
        sleep(20)
        app.buttons["Date And Time"].tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        app.buttons["Save"].tap()
        sleep(60)
 
    }
    func testDetailLANPortFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        let lanport = app.buttons["lanport"].firstMatch
        XCTAssertTrue(lanport.waitForExistence(timeout: 120))
        lanport.tap()

        sleep(5)

        app.buttons["Edit"].tap()
        sleep(5)

        let laniptype = app.buttons["laniptype"].firstMatch
        XCTAssertTrue(laniptype.waitForExistence(timeout: 120))
        laniptype.tap()

        sleep(5)
        
        let laniptypedhcp = app.buttons["laniptypeoneDHCP"].firstMatch
        XCTAssertTrue(laniptypedhcp.waitForExistence(timeout: 120))
        laniptypedhcp.tap()

        sleep(5)
        
        //GateWayflag , GateWayflagFrom DHCP , PriAddrflag , PriAddrflagFrom DHCP , SecAddrflag , SecAddrflagFrom DHCP
        let GateWayflag = app.buttons["GateWayflag"].firstMatch
        XCTAssertTrue(GateWayflag.waitForExistence(timeout: 120))
        GateWayflag.tap()

        sleep(5)
        
        let GateWayflagFromDHCP = app.buttons["GateWayflagFrom DHCP"].firstMatch
        XCTAssertTrue(GateWayflagFromDHCP.waitForExistence(timeout: 120))
        GateWayflagFromDHCP.tap()

        sleep(5)
        
        let PriAddTypeTextDHCP = app.textFields["PriAddTypeTextDHCP"]
        XCTAssertTrue(PriAddTypeTextDHCP.waitForExistence(timeout: 120))
        PriAddTypeTextDHCP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(15)
        
        let PriAddrflag = app.buttons["PriAddrflag"].firstMatch
        XCTAssertTrue(PriAddrflag.waitForExistence(timeout: 120))
        PriAddrflag.tap()

        sleep(5)
        
        let PriAddrflagFromDHCP = app.buttons["PriAddrflagFrom DHCP"].firstMatch
        XCTAssertTrue(PriAddrflagFromDHCP.waitForExistence(timeout: 120))
        PriAddrflagFromDHCP.tap()

        sleep(5)
        
        let SecAddTypeTextDHCP = app.textFields["SecAddTypeTextDHCP"]
        XCTAssertTrue(SecAddTypeTextDHCP.waitForExistence(timeout: 120))
        SecAddTypeTextDHCP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let SecAddrflag = app.buttons["SecAddrflag"].firstMatch
        XCTAssertTrue(SecAddrflag.waitForExistence(timeout: 120))
        SecAddrflag.tap()

        sleep(5)
        
        
        
        let SecAddrflagFromDHCP = app.buttons["SecAddrflagFrom DHCP"].firstMatch
        XCTAssertTrue(SecAddrflagFromDHCP.waitForExistence(timeout: 120))
        SecAddrflagFromDHCP.tap()

        sleep(5)
        
        laniptype.tap()

        sleep(5)

        let laniptypeone = app.buttons["laniptypeoneStatic IP"].firstMatch
        XCTAssertTrue(laniptypeone.waitForExistence(timeout: 120))
        laniptypeone.tap()

        sleep(5)
        
        //SETUP STATIC IP
        
        let ipaddress = app.textFields["ipaddress"]
        XCTAssertTrue(ipaddress.waitForExistence(timeout: 120))
        ipaddress.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        ipaddress.clearText()
        ipaddress.typeText("192.168.18.100")
        sleep(5)
        
        let PriAddTypeText = app.textFields["PriAddTypeText"]
        XCTAssertTrue(PriAddTypeText.waitForExistence(timeout: 120))
        PriAddTypeText.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let SecAddTypeText = app.textFields["SecAddTypeText"]
        XCTAssertTrue(SecAddTypeText.waitForExistence(timeout: 120))
        SecAddTypeText.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let SubnetTypetext = app.textFields["SubnetTypetext"]
        XCTAssertTrue(SubnetTypetext.waitForExistence(timeout: 120))
        SubnetTypetext.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let DHCPserver = app.switches["DHCPserver"].firstMatch
        XCTAssertTrue(DHCPserver.waitForExistence(timeout: 120))
        DHCPserver.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(20)
        DHCPserver.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        
        sleep(5)

        let lanscroll = app.scrollViews["lanscroll"].firstMatch
        XCTAssertTrue(lanscroll.waitForExistence(timeout: 120))
        lanscroll.swipeUp()

        sleep(5)
        
        let startIP = app.textFields["startIP"]
        XCTAssertTrue(startIP.waitForExistence(timeout: 120))
        startIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let secIP = app.textFields["secIP"]
        XCTAssertTrue(secIP.waitForExistence(timeout: 120))
        secIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let LANDHCPServer1 = app.textFields["LANDHCPServer1"]
        XCTAssertTrue(LANDHCPServer1.waitForExistence(timeout: 120))
        LANDHCPServer1.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let LANDHCPServer2 = app.textFields["LANDHCPServer2"]
        XCTAssertTrue(LANDHCPServer2.waitForExistence(timeout: 120))
        LANDHCPServer2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let LANDHCPServer4 = app.textFields["LANDHCPServer4"]
        XCTAssertTrue(LANDHCPServer4.waitForExistence(timeout: 120))
        LANDHCPServer4.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)

        let leasetime = app.buttons["DHCPleasetime"].firstMatch
        XCTAssertTrue(leasetime.waitForExistence(timeout: 120))
        leasetime.tap()
        sleep(5)
        
        let cancelDialog = app.buttons["leasetime0One Month"].firstMatch
        XCTAssertTrue(cancelDialog.waitForExistence(timeout: 120))
        cancelDialog.tap()

        sleep(5)

        app.images["back20"].tap()
        sleep(20)
        lanport.tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)

//        let ipaddress = app.textFields["ipaddress"]
//        XCTAssertTrue(ipaddress.waitForExistence(timeout: 120))
//        ipaddress.tap()
//        sleep(3)
//        app.tap()
//        sleep(5)
//        lanscroll.swipeDown()
//        sleep(3)
        
        //SETUP STATIC IP
        laniptype.tap()
        sleep(5)
        laniptypeone.tap()

        sleep(5)
        ipaddress.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        ipaddress.clearText()
        ipaddress.typeText("192.168.18.100")
        sleep(5)
        let GateWayTypetext = app.textFields["GateWayTypetext"]
        XCTAssertTrue(GateWayTypetext.waitForExistence(timeout: 120))
        GateWayTypetext.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        GateWayTypetext.clearText()
        GateWayTypetext.typeText("0.0.0.0")
        sleep(5)
        app.buttons["Save"].tap()
        sleep(20)
//        app.buttons["Edit"].tap()
//        sleep(5)
        
//        let GateWayTypetext = app.textFields["GateWayTypetext"]
//        XCTAssertTrue(GateWayTypetext.waitForExistence(timeout: 120))
        GateWayTypetext.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        GateWayTypetext.clearText()
        GateWayTypetext.typeText("192.168.18.1")
        sleep(5)
        
        lanscroll.swipeDown()
        sleep(3)
        
        app.buttons["Save"].tap()
        sleep(120)
        
        app.buttons["Edit"].tap()
        sleep(5)
        laniptype.tap()

        sleep(5)
        laniptypedhcp.tap()

        sleep(5)
        app.buttons["Save"].tap()
        sleep(60)
    }
    func testDetailPingFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
         
        sleep(10)
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        app.buttons["Ping Test"].tap()
        let pingIP = app.textFields["pingIP"]
        XCTAssertTrue(pingIP.waitForExistence(timeout: 120))
        pingIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        pingIP.typeText("8.8.8.8")
      //  }
        app.buttons["pingIPExecute"].tap()
        sleep(15)
        
        pingIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        pingIP.clearText()
        sleep(5)
        pingIP.typeText("2404:6800:4012:2::2004")
      //  }
        app.buttons["pingIPExecute"].tap()
        sleep(15)
        
    }
    func testDetailSystemFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        
        XCTAssertTrue(name.waitForExistence(timeout: 120))

        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))//x:143.75, y: 72.0
        sleep(5)
        name.typeText("admin")
        sleep(5)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
   
        app.buttons["Edit"].tap()
        sleep(5)
        
        let prodname = app.textFields["prodname"]
        XCTAssertTrue(prodname.waitForExistence(timeout: 120))
        prodname.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if prodname.isHittable {
            prodname.clearText()
            prodname.typeText("AP00AABBDEDA3A-0706")
        }
        
        app.buttons["Save"].tap()
        sleep(60)
         
    }
    func testDetailTracerouteFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        app.buttons["Traceroute Test"].tap()
        let tracerouteIP = app.textFields["tracerouteIP"]
        XCTAssertTrue(tracerouteIP.waitForExistence(timeout: 120))
        tracerouteIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        tracerouteIP.typeText("8.8.8.8")
      //  }
        app.buttons["tracerouteIPExecute"].tap()

        sleep(20)
    
    }
    func testDetailWiFiDisableFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        app.buttons["wifidisable"].tap()
        sleep(5)
        app.buttons["wifidisableok"].tap()
        sleep(60)
        
    }
    func testDetailWiFiEnableFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        app.buttons["wifienable"].tap()
//        sleep(20)
//        app.buttons["wifienableok"].tap()
        sleep(120)
  
    }
    func testMenuFlow() {
        
        let app = XCUIApplication()
        app.launch()
        
        let Menu = app.images["Menu"]
        XCTAssertTrue(Menu.waitForExistence(timeout: 120))
        Menu.tap()
        
        
        let About = app.staticTexts["menu_About"]
        XCTAssertTrue(About.waitForExistence(timeout: 60))
        About.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(20)
        Menu.tap()
        sleep(5)
        let setting = app.staticTexts["menu_Settings"]
        XCTAssertTrue(setting.waitForExistence(timeout: 60))
        setting.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(10)
        
//        let cantPW = app.switches["cantPW0"]
//        XCTAssertTrue(cantPW.waitForExistence(timeout: 60))
//        cantPW.tap()
//        sleep(5)
//
//        let closepw = app.buttons["closepw"]
//        XCTAssertTrue(closepw.waitForExistence(timeout: 60))
//        closepw.tap()
//        sleep(5)
        
        let OpenfaceID = app.switches["OpenfaceID"]
        XCTAssertTrue(OpenfaceID.waitForExistence(timeout: 60))
        OpenfaceID.tapAtPosition(position: CGPoint(x: 334.3, y: 10.33))
        sleep(5)
        
        let closeFaceErr = app.buttons["closeFaceErr"]
        XCTAssertTrue(closeFaceErr.waitForExistence(timeout: 60))
        closeFaceErr.tap()
        sleep(5)
        
        let OpentouchID = app.switches["OpentouchID"]
        XCTAssertTrue(OpentouchID.waitForExistence(timeout: 60))
        OpentouchID.tapAtPosition(position: CGPoint(x: 334.3, y: 10.33))
        sleep(5)
        
        let closetouch = app.buttons["closetouch"]
        XCTAssertTrue(closetouch.waitForExistence(timeout: 60))
        closetouch.tap()
        sleep(5)
        
//        let cantPW2 = app.switches["cantPW2"]
//        XCTAssertTrue(cantPW2.waitForExistence(timeout: 60))
//        cantPW2.tap()
//        sleep(5)
        
//        OpenfaceID.tap()
//        sleep(10)
        Menu.tap()
        sleep(5)
        
        let goAP = app.buttons["goAP"].firstMatch
        XCTAssertTrue(goAP.waitForExistence(timeout: 120))
        goAP.tap()
        sleep(5)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("123456")
        }
        sleep(5)
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
        sleep(5)
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        password.clearText()
        sleep(5)
        let keep = app.switches["keep"]
        XCTAssertTrue(keep.waitForExistence(timeout: 120))
        keep.tapAtPosition(position: CGPoint(x: 143.75, y: 10.33))
        
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        sleep(5)
        
        login.tap()
       
        sleep(5)
    
        let DetailMenu = app.buttons["DetailMenu"].firstMatch
        XCTAssertTrue(DetailMenu.waitForExistence(timeout: 120))
        DetailMenu.tap()
        
        sleep(10)
       }
    func testUnknow()
    {
        sleep(10)
        let app = XCUIApplication()
        app.launch()
        
        sleep(240) // change wifi
        
        let central = app.buttons["checkLogin00:00:1e:00:01:af"]
        
        XCTAssertTrue(central.waitForExistence(timeout: 120))
        central.tap()
        sleep(20)
       
        app.images["back20"].tap()
        sleep(5)
        
        let Menu = app.images["Menu"]
        XCTAssertTrue(Menu.waitForExistence(timeout: 120))
        Menu.tap()
        
        
        let About = app.staticTexts["menu_About"]
        XCTAssertTrue(About.waitForExistence(timeout: 30))
        About.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(5)
        Menu.tap()
        sleep(5)
        let setting = app.staticTexts["menu_Settings"]
        XCTAssertTrue(setting.waitForExistence(timeout: 30))
        setting.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(5)
        
        let OpenfaceID = app.switches["OpenfaceID"]
        XCTAssertTrue(OpenfaceID.waitForExistence(timeout: 60))
        OpenfaceID.tapAtPosition(position: CGPoint(x: 334.3, y: 10.33))
        sleep(20)
    }
    func testZero() throws
    {
//        sleep(10)
        let app = XCUIApplication()
        app.launch()

        sleep(10)

        let central = app.buttons["checkLogin00:00:1e:00:01:af"]

        XCTAssertTrue(central.waitForExistence(timeout: 120))
        central.tap()
        sleep(10)

        //deleteCentral , deleteAPnotFound , deleteRadioDisable , delete
        //let deletecentral = app.buttons["deleteCentral00:00:1e:00:01:af"]
        let deletecentral = app.buttons["deleteAPnotFound00:00:1e:00:01:af"]
        XCTAssertTrue(deletecentral.waitForExistence(timeout: 120))
        deletecentral.tap()
        sleep(10)

        let apnotfound = app.buttons["checkLogin00:00:2e:00:02:af"]

        XCTAssertTrue(apnotfound.waitForExistence(timeout: 120))
        apnotfound.tap()
        sleep(10)

        //deleteCentral , deleteAPnotFound , deleteRadioDisable , delete
        let deleteAPnotFound = app.buttons["deleteAPnotFound00:00:2e:00:02:af"]

        XCTAssertTrue(deleteAPnotFound.waitForExistence(timeout: 120))
        deleteAPnotFound.tap()
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
//        sleep(5)
//
//        let name = app.textFields["username"]
//        XCTAssertTrue(name.waitForExistence(timeout: 120))
//        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
//        name.typeText("admin")
//
//        sleep(2)
//
//        let password = app.secureTextFields["password"]
//        XCTAssertTrue(password.waitForExistence(timeout: 120))
//        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
//        if password.isHittable {
//            password.typeText("1234")
//        }
//        let login = app.buttons["Login"].firstMatch
//        XCTAssertTrue(login.waitForExistence(timeout: 120))
//        login.tap()
       
        sleep(10)
        
        let delete = app.buttons["delete00:AA:BB:DE:DA:3A"] //00:AA:BB:CC:DD:10

        XCTAssertTrue(delete.waitForExistence(timeout: 120))
        delete.tap()
        sleep(10)
        
    }*/
    func takeScreenshot(name: String) {
      let fullScreenshot = XCUIScreen.main.screenshot()

      let screenshot = XCTAttachment(uniformTypeIdentifier: "public.png", name: "Screenshot-\(name)-\(UIDevice.current.name).png", payload: fullScreenshot.pngRepresentation, userInfo: nil)
      screenshot.lifetime = .keepAlways
      add(screenshot)
    }
}
extension XCUIElement {
    var isFocused: Bool {
        let isFocused = (self.value(forKey: "hasKeyboardFocus") as? Bool) ?? false
        return isFocused
    }
    func tap(wait: Int, test: XCTestCase) {
            if !isHittable {
                test.expectation(for: NSPredicate(format: "hittable == true"), evaluatedWith: self, handler: nil)
                test.waitForExpectations(timeout: TimeInterval(wait), handler: nil)
            }
            tap()
     }
    func clearText() {
            guard let stringValue = self.value as? String else {
                return
            }

            var deleteString = String()
            for _ in stringValue {
                deleteString += XCUIKeyboardKey.delete.rawValue
            }
            typeText(deleteString)
        }
}
class Springboard {
   class func deleteApp () {
       let springboard = XCUIApplication(bundleIdentifier: "com.acelink.PSTemplate")
       //terminate app and activate Springboard
       XCUIApplication().terminate()
       springboard.activate()
       
       //tap on app icon
       let appIcon = springboard.icons.matching(identifier: "Security").firstMatch
       
       if appIcon.exists {
           
       appIcon.press(forDuration: 2.0)
           
          //Access first alert button (Remove App)
       let _ = springboard.alerts.buttons["Remove App"].waitForExistence(timeout: 1.0)
       springboard.buttons["Remove App"].tap()
       
       //Access second alert button (Delete App)
       let _ = springboard.alerts.buttons["Delete App"].waitForExistence(timeout: 1.0)
       springboard.buttons["Delete App"].tap()
       
       //Access second alert button (Delete)
       let _ = springboard.alerts.buttons["Delete"].waitForExistence(timeout: 1.0)
       springboard.buttons["Delete"].tap()

       }
   }
}
//class Springboard {
//
//   static let springboard = XCUIApplication(bundleIdentifier: "com.acelink.PSTemplate")
//
//   class func deleteApp () {
//       //terminate app and activate Springboard
//       XCUIApplication().terminate()
//       springboard.activate()
//
//       //tap on app icon
//       let appIcon = springboard.icons.matching(identifier: "Security").firstMatch
//
//       if appIcon.exists {
//
//       appIcon.press(forDuration: 2.0)
//
//          //Access first alert button (Remove App)
//       let _ = springboard.alerts.buttons["Remove App"].waitForExistence(timeout: 1.0)
//       springboard.buttons["Remove App"].tap()
//
//       //Access second alert button (Delete App)
//       let _ = springboard.alerts.buttons["Delete App"].waitForExistence(timeout: 1.0)
//       springboard.buttons["Delete App"].tap()
//
//       //Access second alert button (Delete)
//       let _ = springboard.alerts.buttons["Delete"].waitForExistence(timeout: 1.0)
//       springboard.buttons["Delete"].tap()
//
//       }
//   }
//}
//class Springboard {
//
//    static let springboard = XCUIApplication(bundleIdentifier: "com.acelink.PSTemplate")
//
//    /**
//     Terminate and delete the app via springboard
//     */
//    class func deleteMyApp() {
//        XCUIApplication().terminate()
//
//         // Force delete the app from the springboard
//        let icon = springboard.icons["Security"]
//        let a = springboard.icons
//        if icon.exists {
//            let iconFrame = icon.frame
//            let springboardFrame = springboard.frame
//            icon.press(forDuration: 1.3)
//
//            // Tap the little "X" button at approximately where it is. The X is not exposed directly
//            springboard.coordinate(withNormalizedOffset: CGVector(dx: (iconFrame.minX + 3) / springboardFrame.maxX, dy: (iconFrame.minY + 3) / springboardFrame.maxY)).tap()
//
//            springboard.alerts.buttons["Delete"].tap()
//        }
//    }
// }
extension XCUIApplication {
    func uninstall(name: String? = nil) {
        self.terminate()

        let timeout = TimeInterval(10)
        let springboard = XCUIApplication(bundleIdentifier: "com.acelink.PSTemplate")

        let appName: String
        if let name = name {
            appName = name
        } else {
            let uiTestRunnerName = Bundle.main.infoDictionary?["CFBundleName"] as! String
            appName = uiTestRunnerName.replacingOccurrences(of: "UITests-Runner", with: "")
        }

        /// use `firstMatch` because icon may appear in iPad dock
        let appIcon = springboard.icons[appName].firstMatch
        if appIcon.waitForExistence(timeout: timeout) {
            appIcon.press(forDuration: 2)
        } else {
            XCTFail("Failed to find app icon named \(appName)")
        }

        let removeAppButton = springboard.buttons["Remove App"]
        if removeAppButton.waitForExistence(timeout: timeout) {
            removeAppButton.tap()
        } else {
            XCTFail("Failed to find 'Remove App'")
        }

        let deleteAppButton = springboard.alerts.buttons["Delete App"]
        if deleteAppButton.waitForExistence(timeout: timeout) {
            deleteAppButton.tap()
        } else {
            XCTFail("Failed to find 'Delete App'")
        }

        let finalDeleteButton = springboard.alerts.buttons["Delete"]
        if finalDeleteButton.waitForExistence(timeout: timeout) {
            finalDeleteButton.tap()
        } else {
            XCTFail("Failed to find 'Delete'")
        }
    }
}
extension XCUIElement {
    func forceTap() {
//        if self.isHittable {
//            self.tap()
//        } else {
            let coordinate: XCUICoordinate = self.coordinate(withNormalizedOffset: CGVector(dx: 0.0, dy: 0.0))
            coordinate.tap()
//        }
    }
    func tapAndWaitForKeyboardToAppear() {
        let waitTime = 1.0
        let retryMaxCount = 20
        let keyboard = XCUIApplication().keyboards.element
        for _ in 0..<retryMaxCount {
            tap()
            if keyboard.waitForExistence(timeout: waitTime) {
                return
            }
        }
        XCTAssert(false,"keyboard failed to appear")
    }
    func tapAtPosition(position:CGPoint) {
        let coor = self.self.coordinate(withNormalizedOffset: CGVector(dx: 0.0, dy: 0.0)).withOffset(CGVector(dx: position.x, dy: position.y))
        coor.tap()
    }
}
extension XCTestCase {
    func waitAndCheck(_ description: String = "", _ timeout: Double = 0.5, callback: () -> Bool) {
        let exp = self.expectation(description: description)
        let result = XCTWaiter.wait(for: [exp], timeout: timeout)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertTrue(callback())
        }
        else {
            XCTFail("Timeout waiting \(timeout) for \(description)")
        }
    }

    func wait(_ description: String = "", _ timeout: Double = 0.5, callback: () -> Void) {
        let exp = self.expectation(description: description)
        let result = XCTWaiter.wait(for: [exp], timeout: timeout)
        if result == XCTWaiter.Result.timedOut {
            callback()
        }
        else {
            XCTFail("Timeout waiting \(timeout) for \(description)")
        }
    }
}
