//
//  ManageViews.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/24.
//

import SwiftUI

class ManageViews: ObservableObject {
    @Published var page = 0 {
        willSet {
            self.objectWillChange.send()
        }
    }
}

