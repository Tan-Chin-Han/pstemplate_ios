//
//  NavigationItemContainer.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/27.
//

import SwiftUI

struct NavigationItemContainer: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct NavigationItemContainer_Previews: PreviewProvider {
    static var previews: some View {
        NavigationItemContainer()
    }
}
