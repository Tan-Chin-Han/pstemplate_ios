import SwiftUI
import CoreData
import Combine
struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    @Environment(\.managedObjectContext) private var viewContext
    @StateObject var backPress = ManageViews.shared
    @StateObject var mLocalAuth = SSBKeychain()
    @StateObject var datas = ManageDatas()
    @State var show = true
    @State var backgroung = false
    @State var client = SSDPDiscovery()
    @State var mSSDPResult = 0 //20220418 enable SSDP function
    @State private var flag = false
    @State var unknpwn = 0
    @State var locationManager = LocationManager()
    @State private var date1: Date = Date()
    @State private var date2: Date = Date()
    var body: some View {
        NavigationView {
            showMainView()
                .onReceive(Just(mSSDPResult)) { _ in
                    if backPress.page != 0 && !flag && !backPress.rebootFlag {
                            backPress.mSSDPSearchAlert = true
                            self.sSDPsearch()
                    }
                }
         }
        .onAppear {
            for data in datas.checkMyAPAmount() {
                if data.key != "" {
                    do {
                        let result = (try? datas.decodeLocalStore(from: data.value))! as SSDP
                        myAP(index:0, myap: nil, newap: nil, ssdp: nil,ssdpI:result, unknow:7)
                        backPress.newFlag = true
                        backPress.myFlag = true
                    }
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .environmentObject(backPress)
        .preferredColorScheme(.dark)
        .onChange(of: scenePhase) { newPhase in
            if newPhase == .active {
                print("Active")
                backgroung = false
                mLocalAuth.checkAuth()
                date2 = Date()
                let nInterval = date2.timeIntervalSince(date1)
                if nInterval > 60 {
                    unknpwn = 6
                    backPress.newFlag = true
                    backPress.page = 1
                }
            }
            else if newPhase == .inactive {
                print("Inactive")
                backgroung = true
                mLocalAuth.checkAuth()
            }
            else if newPhase == .background {
                print("Background")
                backgroung = true
                mLocalAuth.checkAuth()
                date1 = Date()
            }
        }
    }
    @ViewBuilder
    func showMainView() -> some View {
        if backPress.page == 0 {
            Text("").fullScreenCover(isPresented: $show, content: {
                MainView.init()
            })
        }
        else if backPress.page == 1 {
            VStack(spacing: 20) {
                Text("").fullScreenCover(isPresented: $show, content: {
                    ZStack {
                        AppTemplate.init(presented: $show)
                            .opacity(backgroung ? 0.0:1.0)
                        goBackgroung()
                    }
                })
            }
        }
        else if backPress.page == 2 {
            VStack(spacing: 20) {
                Text("").fullScreenCover(isPresented: $show, content: {
                    ZStack {
                        AppDetail.init(presented: $show)
                        goBackgroung()
                    }
                })
            }
        }
    }
    @ViewBuilder
    func goBackgroung()->some View {
        GeometryReader { geometry in
        VStack {
            HStack {Text("")}
            .frame(height: abs(geometry.size.height/2 - 120))
            Image("WiFi6")
                .resizable()
                .frame(width: 69, height:60 )
            Text("Wireless")
                .font(.InterSemiBold36)
                .foregroundColor(Color(hex:0xFFFFFF))
            HStack {Text("")}
            .frame(height: abs(geometry.size.height/2 - 30))
            .background(Color.clear)
            Image("SophosAP")
                .padding(.bottom,60)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.c0x005CC8)
        .edgesIgnoringSafeArea(.all)
        .navigationBarHidden(true)
        .opacity(backgroung ? 1.0:0.0)
        }
    }
    func sSDPsearch() {
        if !flag {
            flag = true
            mSSDPResult = 0
            client.mSSDPResults.removeAll(keepingCapacity: true)
            NSLog("----SSDP discoverService seconds----")
            client.discoverService(forDuration: 3.0, searchTarget: "www.sophos.com:device:WLANAccessPointDevice:1", port: 1900)
            DispatchQueue.main.asyncAfter(deadline: .now() + 13.0) {
                //Start 3次 Discover
                NSLog("10 seconds")
                flag = false
                mSSDPResult = 1
            }
           DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
               NSLog("3 seconds")
               backPress.mSSDPSearchAlert = false
               backPress.mSSDPDiscvoer = client.mSSDPResults
               if client.mSSDPResults.isEmpty && unknpwn <= 5 {
                   unknpwn += 1
               }
               if unknpwn == 6 {
                   for apuniq in 0..<backPress.myAccess.count {
                       myAP(index:0, myap: backPress.myAccess[apuniq], newap: nil, ssdp: nil,ssdpI:nil, unknow:7)
                   }
               }
                //filter 3次 Discover 結果
               if backPress.myAccess.isEmpty {
                   for respone in client.mSSDPResults {
                       unknpwn = 0
                        let result = SSDPService(host: respone.key, response: respone.value)
                        if result.mac != nil && backPress.newAccess.filter({$0.mac == result.mac}).isEmpty && backPress.myAccess.filter({$0.mac == result.mac}).isEmpty {
                            //420 系列 connected user 0-256 220 219 141
                            //840 系列 connected user 0-512 440 439 282
                            myAP(index: 0, myap: nil, newap: "", ssdp: result,ssdpI:nil,unknow: nil)
                            backPress.newFlag = true
                               
                        }
                    }//for
               }
               else {
               for apuniq in 0..<backPress.myAccess.count {
               var check = false
               for respone in client.mSSDPResults {
                   unknpwn = 0
                    let result = SSDPService(host: respone.key, response: respone.value)
                    if result.mac != nil {
                        if backPress.newAccess.filter({$0.mac == result.mac}).isEmpty && backPress.myAccess.filter({$0.mac == result.mac}).isEmpty {
                                //420 系列 connected user 0-256 220 219 141
                            //840 系列 connected user 0-512 440 439 282
                            myAP(index: 0, myap: nil, newap: "", ssdp: result,ssdpI:nil,unknow: nil)
                            backPress.newFlag = true
                        }
                        else {
                            if backPress.myAccess[apuniq].mac == result.mac {
                                myAP(index: apuniq, myap: nil,newap:nil,ssdp: result,ssdpI:nil,unknow: nil)
                                check = true
                                NSLog("seconds update SSDP disvover")
                            }
                        }//else
                    }
                }//for
               if !check {
                   myAP(index: apuniq, myap: backPress.myAccess[apuniq],newap:nil,ssdp: nil,ssdpI: nil,unknow: nil)
               }
               }//for
               }//else
            }//DispatchQueue.main.asyncAfter(deadline: .now() + 3.0)
        } // if flag == false
    }
    func myAP(index:Int,myap:MyAccessBody?,newap:String?,ssdp:SSDPService?,ssdpI:SSDP?,unknow:Int?) {
        if myap != nil {
                var health = backPress.myAccess[index].health
                if (myap?.unknow ?? 0) == 5 {
                    health = ""
                }
                let appname = myap?.appname ?? "rename"
                let mac = myap?.mac ?? ""
                let clients = myap?.client ?? "10"
                let location = myap?.location ?? ""
                let central = myap?.central ?? "0"
                let wifiradio = myap?.wifiradio ?? "3"
                let model = myap?.model ?? ""
                let unknow = (myap?.unknow ?? 0) + 1
                let amyap = MyAccessBody(appname:appname, mac:mac, health:health, client:clients, location:location, central: central ,wifiradio:wifiradio,model:model,unknow:unknow)
                backPress.myAccess[index] = amyap
        }
        else if myap != nil && unknow != nil {
            let appname = myap?.appname ?? ""
            let mac = myap?.mac ?? ""
            let clients = myap?.client ?? "10"
            let location = myap?.location ?? ""
            let central = myap?.central ?? "0"
            let wifiradio = myap?.wifiradio ?? "3"
            let model = myap?.model ?? ""
            let amyap = MyAccessBody(appname:appname, mac:mac, health:"", client:clients, location:location, central: central, wifiradio:wifiradio, model:model,unknow: 7)
            backPress.myAccess[index] = amyap
        }
        else if newap != nil && ssdp != nil {
            let mCPU = Int(ssdp?.cPUUtilization ?? "0")!
            let mMemory = Int(ssdp?.memoryUtilization ?? "0")!
            let mConnected = Int(ssdp?.connectedUsers ?? "0")!
            var health = "Excellent"
            if ((ssdp?.model?.contains("420")) != nil) {
                if mCPU >= 86 || mMemory >= 86 || mConnected >= 220 {
                    health = "Poor"
                }
                else if (mCPU <= 85 && mCPU >= 56 ) || (mMemory <= 85 && mMemory >= 56 ) || ( mConnected <= 219 && mConnected >= 141 ) {
                    health = "Good"
                }
            }
            else if ((ssdp?.model?.contains("840")) != nil) {
                if mCPU >= 86 || mMemory >= 86 || mConnected >= 440 {
                    health = "Poor"
                }
                else if (mCPU <= 85 && mCPU >= 56 ) || (mMemory <= 85 && mMemory >= 56 ) || ( mConnected <= 439 && mConnected >= 282 ) {
                    health = "Good"
                }
            }
            let appname = ssdp?.name ?? "rename"
            let mac = ssdp?.mac ?? ""
            let clients = ssdp?.connectedUsers ?? "10"
            let location = ssdp?.location ?? ""
            let central = ssdp?.operatorMode ?? "0"
            backPress.newAccess.append(NewAccessBody(appname:appname,mac:mac,health:health,client:clients,location:location,central:central,wifiradio:"3",model:"420",unknow:0))
        }
        else if ssdp != nil {
            let mCPU = Int(ssdp?.cPUUtilization ?? "0")!
            let mMemory = Int(ssdp?.memoryUtilization ?? "0")!
            let mConnected = Int(ssdp?.connectedUsers ?? "0")!
            var health = "Excellent"
            if mCPU >= 86 || mMemory >= 86 || mConnected >= 220 {
                health = "Poor"
            }
            else if (mCPU <= 85 && mCPU >= 56 ) || (mMemory <= 85 && mMemory >= 56 ) {
                health = "Good"
            }
            let appname = ssdp?.name ?? "rename"
            let mac = ssdp?.mac ?? ""
            let clients = ssdp?.connectedUsers ?? "10"
            let location = ssdp?.location ?? ""
            let central = ssdp?.operatorMode ?? "0"
            let wifiradio = ssdp?.wiFiRadio ?? "3"
            let model = ssdp?.model ?? ""
            let amyap = MyAccessBody(appname:appname,mac:mac,health:health,client:clients,location: location,central:central,wifiradio:wifiradio,model:model,unknow:0)
            backPress.myAccess[index] = amyap
        }
        else if ssdpI != nil {
            let appname = ssdpI?.Name ?? "rename"
            let mac = ssdpI?.Mac ?? ""
            let health = ssdpI?.CPUUtilization ?? "Good"
            let clients = ssdpI?.ConnectedUsers ?? "10"
            let location = ssdpI?.LOCATION
            let central = ssdpI?.OperatorMode
            let wifiradio = ssdpI?.WiFiRadio
            let model = ssdpI?.Model
            backPress.myAccess.append(MyAccessBody(appname:appname, mac:mac, health:health, client:clients, location:location, central:central, wifiradio:wifiradio, model: model, unknow:7))
        }
    }
}
