import SwiftUI
import os

@main
struct PSTemplateApp: App {
    @StateObject var backPress = ManageViews.shared
    var body: some Scene {
        WindowGroup {
         ContentView()
            .environmentObject(backPress)
            .onAppear() {
                AntiJailbreak.init().assignJailBreakCheckType(type: .readAndWriteFiles)
                AntiJailbreak.init().assignJailBreakCheckType(type: .systemCalls)
            }
        }
    }
}
