//
//  PSNetwork.swift
//  pslib
//
//  Created by Edimax on 2021/10/28.
//

import SwiftUI
import Network
import Darwin
import OSLog

class PSNetwork: ObservableObject {
    
    var url:String?
    var method:String?
    var data:Data?
    
   // @Published
    var result:Data?
    @Published var t:Decodable? {
        didSet {
            self.objectWillChange.send()
        }
    }
    
    func getResult<T:Decodable>(model: T.Type){
        let _url = URL(string: self.url!)
        var request = URLRequest(url: _url!)
        request.httpMethod = self.method
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = self.data
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let datas = data else{ return }
            os_log("\(String(describing:datas))")
            
            DispatchQueue.main.async { [self] in //background
                self.result = datas
                
                do{
                    self.t  = try model.init(jsonData: self.result!)
                    
                    
                } catch {
                    print(error)
                    
                }
            }
            
        }.resume()
        
        
    }
   
}
extension Decodable {
    init(jsonData: Data) throws {
        self = try JSONDecoder().decode(Self.self, from:jsonData)
    }
}
