//
//  SSBKeychain.swift
//  pslib
//
//  Created by Edimax on 2021/10/28.
//

import SwiftUI
import CryptoKit
import Security
import LocalAuthentication

class SSBKeychain: ObservableObject {

    @Published
    var loggedIn: Bool = false
    @Published
    var hasChanges: Bool = false

    private var domainState: Data?

    func auth() {
        let context = LAContext()
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Biometric Auth") {
            [weak self] (res, err) in
            DispatchQueue.main.async {
                self?.loggedIn = res
            }
        }
    }

    func check() {
        let context = LAContext()
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        checkDomainState(context.evaluatedPolicyDomainState)
    }

    private func checkDomainState(_ domainState: Data?) {
        if let `domainState` = domainState {
            if domainState != self.domainState {
                hasChanges = true
            } else {
                hasChanges = false
            }
        }
        self.domainState = domainState
        hasChanges = true
    }

}
public enum KeychainWrapperError: Error {
    case emptyKeyOrValue
    case failure(status: OSStatus)
}

class PSKeyStore {
    
    public func set(string: String, forKey key: String) throws {
        guard !string.isEmpty && !key.isEmpty else {
            throw KeychainWrapperError.emptyKeyOrValue
        }
        
//        do{
//            try removeString(forKey: key)
//        } catch {
//            throw error
//        }
        
        var qd = setupQueryDictionary(forKey: key)
        qd[kSecValueData as String] = string.data(using: .iso2022JP)
        qd[kSecAttrAccessible as String] = kSecAttrAccessibleWhenUnlocked
        
        let status = SecItemAdd(qd as CFDictionary, nil)
        
        if status != errSecSuccess {
            throw KeychainWrapperError.failure(status: status)
        }
    }
    
    public func get(forKey key: String) throws -> String? {
        guard !key.isEmpty else {
            throw KeychainWrapperError.emptyKeyOrValue
        }
        
        var qd = setupQueryDictionary(forKey: key)
        qd[kSecReturnData as String] = kCFBooleanTrue
        qd[kSecMatchLimit as String] = kSecMatchLimitOne
        
        var data: AnyObject?
        let status = SecItemCopyMatching(qd as CFDictionary, &data)
        
        if status != errSecSuccess {
            throw KeychainWrapperError.failure(status: status)
        }
        
        let result: String?
        if (data as? Data) != nil {
            result = String(data: data as! Data, encoding: .utf8)
        } else {
            result = nil
        }
        return result
    }
    
    public func removeString(forKey key: String) throws  {
        guard !key.isEmpty else {
            throw KeychainWrapperError.emptyKeyOrValue
        }
        
        let qd = setupQueryDictionary(forKey: key)
        let status = SecItemDelete(qd as CFDictionary)
        
        if status != errSecSuccess {
            throw KeychainWrapperError.failure(status: status)
        }
    }
    
    private func setupQueryDictionary(forKey key: String) -> [String: Any] {
        var queryDictionary:[String: Any] = [kSecClass as String: kSecClassGenericPassword]
        
        queryDictionary[kSecAttrAccount as String] = key.data(using: .utf8)
        
        return queryDictionary
    }
    
}
