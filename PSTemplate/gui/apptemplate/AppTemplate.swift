//
//  AppTemplate.swift
//  PSTemplate
//
//  Created by Edimax on 2022/1/18.
//

import SwiftUI

struct AppTemplate: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct AppTemplate_Previews: PreviewProvider {
    static var previews: some View {
        AppTemplate()
    }
}
