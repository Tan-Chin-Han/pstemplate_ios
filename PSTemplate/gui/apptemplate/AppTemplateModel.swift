//
//  AppTemplateModel.swift
//  PSTemplate
//
//  Created by Edimax on 2022/1/20.
//

import SwiftUI

struct AppTemplateModel: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct AppTemplateModel_Previews: PreviewProvider {
    static var previews: some View {
        AppTemplateModel()
    }
}
