//
//  AppTemplateModel.swift
//  PSTemplate
//
//  Created by Edimax on 2022/1/20.
//

import SwiftUI

import Foundation

struct MyAccessBody: Encodable, Hashable {
    let appname:String?
    let mac:String?
    let health:String?
    
    init(appname: String?, mac: String?,health:String?) {
        self.appname = appname
        self.mac = mac
        self.health = health
    }
}
struct MyAccessResponse: Decodable {
    enum CodingKeys: String, CodingKey {
            case appname, mac, id , health   }
    let appname:String?
    let mac:String?
    let health:String?
    let id:String?
    
    init(appname: String?, mac: String?,health:String?,id: String?) {
        self.appname = appname
        self.mac = mac
        self.health = health
        self.id = id
    }
}
struct NewAccessBody: Encodable, Hashable {
    let appname:String?
    let mac:String?
    let health:String?
    
    init(appname: String?, mac: String?,health:String?) {
        self.appname = appname
        self.mac = mac
        self.health = health
    }
}
struct NewAccessResponse: Decodable {
    enum CodingKeys: String, CodingKey {
            case appname, mac, id , health   }
    let appname:String?
    let mac:String?
    let health:String?
    let id:String?
    
    init(appname: String?, mac: String?,health:String?,id: String?) {
        self.appname = appname
        self.mac = mac
        self.health = health
        self.id = id
    }
}
