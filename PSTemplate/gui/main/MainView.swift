//
//  MainView.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/24.
//

import SwiftUI

struct MainView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var test = TestViewModel()
    @State var fullScreen = false
    @State var show = false
    
    @EnvironmentObject var backPress : ManageViews
    
    @State private var thing: String = "Sophos"
    @State private var showPopup: Bool = false
    
    @State private var location: CGPoint = CGPoint(x: 0, y: 0)
    //@Binding private var name = ""
    
    var simpleDrag: some Gesture {
        DragGesture()
            .onChanged { value in
                self.location = value.location
            }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                
                showMessage()
                Text(test.addUserResult?.name ?? "")
                    .frame(width: 300, height: 30, alignment: .topLeading)
                    .background(Color.blue)
                Text(thing)
                    .frame(width: 300, height: 30, alignment: .topLeading)
                    
                NavigationLink(destination: TestView(presented: $fullScreen), label: { Text("next") }).background(Color.red)
                
                Button(action: {
                    self.fullScreen = true
                    //ContentView(test: self.test)
                    backPress.page = 1
                    //ContentView(test: self.test)
                  //  presentationMode.wrappedValue.dismiss()
                  //  ContentView().show = true
                }, label: {
                    Text("go to another view")
                })
//                .fullScreenCover(isPresented: $fullScreen, content: {
//                    TestView.init(presented: $fullScreen)
//                })
                
                VStack {
                    Image(systemName: "star")
                    Text("Hello World!")
                }
                .hoverEffect()
                .onTapGesture {
                    self.show.toggle()
                    showPopup = true
                }
                
                showDialog()
            }
            .navigationBarHidden(true)
            
        }
        .onAppear(){
            
            test.addUser()
        }
        .onReceive(test.$addUserResult ) { (result) in
          if result != nil {
            print(result!.name!)
            //name = result!.name!
            //ContentView(name: name)
          }
        
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .environmentObject(test)
    }
    @ViewBuilder
    func showMessage() -> some View {
        if show {
            Text("Error !!")
            .foregroundColor(Color.white)
            .background(Color.red)
            .cornerRadius(5)
             
        }
    }
    @ViewBuilder
    func showDialog() -> some View {
        if showPopup {
            ZStack(){}.popup(isPresented: $showPopup, with: $thing) { item in
               VStack(spacing: 20) {
                TextField("Name", text: item)
                   Button {
                    showPopup = false
                    show = false
                   } label: {
                       Text("Dismiss Popup")
                   }
                   HStack {
                       Image(systemName: "star")
                       Text("Hello World!")
                   }
               }
               .frame(width: 200)
               .padding()
               .background(Color.green)
               .cornerRadius(8)
               .position(location)
               .gesture(simpleDrag)
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
