//
//  TestViewModel.swift
//  pslib
//
//  Created by Edimax on 2021/12/22.
//
import SwiftUI
import os
import Combine

class MainViewModel:ObservableObject {
    @ObservedObject var request: PSNetwork = PSNetwork()
    var anyCancellable = Set<AnyCancellable>()
    private var responses = 0
    @Published var response = 0
}
