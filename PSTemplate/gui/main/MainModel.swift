//
//  TestModel.swift
//  pslib (iOS)
//
//  Created by Edimax on 2021/12/21.
//
import Foundation

struct SSDP: Codable, Hashable  {
    let LOCATION:String?
    let SERVER:String?
    let ST:String?
    let USN:String?
    let Vendor: String?
    let Mac: String?
    let Model: String?
    let Name: String?
    let OperatorMode: String?
    let Identity: String?
    let FirmwareVersion: String?
    let CPUUtilization: String?
    let ConnectedUsers: String?
    let SNR: String?
    let MemoryUtilization: String?
    let WiFiRadio: String?
    let live: String?
    let SOPHOS_INFO: String?
    
    init(LOCATION: String?, SERVER: String?,ST:String?,USN:String?,Vendor: String?,Mac: String?,Model: String?,Name: String?,OperatorMode: String?,Identity: String?,FirmwareVersion: String?,CPUUtilization: String?,ConnectedUsers: String?,SNR: String?,MemoryUtilization: String?,WiFiRadio: String?,live: String?,SOPHOS_INFO: String?) {
        self.LOCATION = LOCATION
        self.SERVER = SERVER
        self.ST = ST
        self.USN = USN
        self.Vendor = Vendor
        self.Mac = Mac
        self.Model = Model
        self.Name = Name
        self.OperatorMode = OperatorMode
        self.Identity = Identity
        self.FirmwareVersion = FirmwareVersion
        self.CPUUtilization = CPUUtilization
        self.ConnectedUsers = ConnectedUsers
        self.SNR = SNR
        self.MemoryUtilization = MemoryUtilization
        self.WiFiRadio = WiFiRadio
        self.live = live
        self.SOPHOS_INFO = SOPHOS_INFO
    }
}
