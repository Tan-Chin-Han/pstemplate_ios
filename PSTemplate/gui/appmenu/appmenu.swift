//
//  appmenu.swift
//  PSTemplate
//
//  Created by Edimax on 2022/1/18.
//

import SwiftUI

struct appmenu: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct appmenu_Previews: PreviewProvider {
    static var previews: some View {
        appmenu()
    }
}
