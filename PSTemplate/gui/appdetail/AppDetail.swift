//
//  AppDetail.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/1/25.
//

import SwiftUI

struct AppDetail: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct AppDetail_Previews: PreviewProvider {
    static var previews: some View {
        AppDetail()
    }
}
