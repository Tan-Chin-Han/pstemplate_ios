//
//  AppSubView.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/3/24.
//

import SwiftUI

struct AppSubView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct AppSubView_Previews: PreviewProvider {
    static var previews: some View {
        AppSubView()
    }
}
